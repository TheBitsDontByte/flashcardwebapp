USE Northwind;

SELECT DISTINCT(o.OrderID), OrderDate, CompanyName
FROM Orders o 
	INNER JOIN Customers c ON o.CustomerID = c.CustomerID
	INNER JOIN Order_Details od ON o.OrderID = od.OrderID
WHERE od.ProductID IN 
    -- subquery
	(SELECT ProductID
 	 FROM Products
         WHERE UnitPrice > 90.00
	 ORDER BY UnitPrice DESC)
ORDER BY CompanyName, OrderID;