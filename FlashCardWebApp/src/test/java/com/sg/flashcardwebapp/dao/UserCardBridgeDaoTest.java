/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.dao;

import com.sg.flashcardwebapp.model.Card;
import com.sg.flashcardwebapp.model.User;
import java.time.LocalDateTime;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author chris
 */
public class UserCardBridgeDaoTest {

    FlashCardDao cardDao;
    UserDao userDao;

    UserCardBridgeDao dao;
    User user;
    Card card;

    public UserCardBridgeDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("userCardBridgeDao", UserCardBridgeDao.class);
        cardDao = ctx.getBean("cardDao", FlashCardDao.class);
        userDao = ctx.getBean("userDao", UserDao.class);

        user = new User();
        card = new Card();

        user.setPassword("pw");
        user.setUserName("Chris");
        card.setQuestion("What is pi");
        card.setAnswer("3.14");
        card.setTag("math");
        card.setAddedDate(LocalDateTime.now());
        card.setStudiedDate(LocalDateTime.now());
        card.setNextStudyDate(LocalDateTime.now().plusDays(3));

        List<Card> cards = cardDao.getAllCards();
        List<User> users = userDao.getAllUsers();
        for (User u : users) {
            List<Card> userCards = dao.getAllCardsForUser(u.getUserId());
            for (Card c : userCards) {
                dao.deleteConnection(u.getUserId(), c.getCardId());
            }
        }

        for (User u : users) {
            userDao.deleteUser(u.getUserId());
        }

        for (Card c : cards) {
            cardDao.removeCard(c.getCardId());
        }

        user = userDao.addUser(user);
        card = cardDao.addCard(card);

    }

    @After
    public void tearDown() {
        List<Card> cards = cardDao.getAllCards();
        List<User> users = userDao.getAllUsers();
        for (User u : users) {
            List<Card> userCards = dao.getAllCardsForUser(u.getUserId());
            for (Card c : userCards) {
                dao.deleteConnection(u.getUserId(), c.getCardId());
            }
        }

        for (User u : users) {
            userDao.deleteUser(u.getUserId());
        }

        for (Card c : cards) {
            cardDao.removeCard(c.getCardId());
        }

    }

    @Test
    public void testAddDeleteGetAllConnection() {
        dao.createConnection(user.getUserId(), card);
        List<Card> userCards = dao.getAllCardsForUser(user.getUserId());
        assertEquals(1, userCards.size());
        dao.deleteConnection(user.getUserId(), card.getCardId());
        userCards = dao.getAllCardsForUser(user.getUserId());
        assertEquals(0, userCards.size());

    }

    @Test
    public void testGetNextNewCard() {
        dao.createConnection(user.getUserId(), card);
        Integer cardId = dao.getNextNewCardId(user.getUserId());
        assertEquals((int) cardId, card.getCardId());

    }

    @Test
    public void testGetNextCard() {
        dao.createConnection(user.getUserId(), card);
        Integer cardId = dao.getNextStudiedCardId(user.getUserId());
        assertNull(cardId);

    }

    @Test
    public void testUpdateTime() {
        dao.createConnection(user.getUserId(), card);
        LocalDateTime ld = LocalDateTime.parse("2007-12-03T10:15:30");
        card.setStudiedDate(ld);
        card.setNextStudyDate(ld.plusDays(1));
        dao.updateStudyDate(user.getUserId(), card);
        int cardId = dao.getNextStudiedCardId(user.getUserId());
        assertEquals(card.getCardId(), cardId);

    }

    @Test
    public void testGetNumOfCard() {
        int numOfCards = dao.getNumOfCardToStudy(user.getUserId());
        assertEquals(0, numOfCards);
        dao.createConnection(user.getUserId(), card);
        LocalDateTime ld = LocalDateTime.now();
        card.setNextStudyDate(ld);
        dao.updateStudyDate(user.getUserId(), card);
        numOfCards = dao.getNumOfCardToStudy(user.getUserId());
        assertEquals(1, numOfCards);

    }

}
