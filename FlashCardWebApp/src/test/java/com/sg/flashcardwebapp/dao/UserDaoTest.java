/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.dao;

import com.sg.flashcardwebapp.model.User;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author chris
 */
public class UserDaoTest {

    UserDao dao;
    User user;

    public UserDaoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("userDao", UserDao.class);

        List<User> allUsers = dao.getAllUsers();
        for (User u : allUsers) {
            dao.deleteUser(u.getUserId());
        }

        user = new User();
        user.setUserName("Chris");
        user.setPassword("pw");

    }

    @After
    public void tearDown() {
        List<User> allUsers = dao.getAllUsers();
        for (User u : allUsers) {
            dao.deleteUser(u.getUserId());
        }
    }

    @Test
    public void testAdd() {
        assertEquals(0, user.getUserId());
        dao.addUser(user);
        assertTrue(user.getUserId() != 0);
    }

    @Test
    public void testGet() {
        dao.addUser(user);
        User newUser = dao.getUser(user.getUserId());
        assertTrue(newUser.getUserName().equals(user.getUserName()));
    }

    @Test
    public void testUpdate() {
        dao.addUser(user);
        user.setUserName("XXX");
        dao.updateUser(user);
        User newUser = dao.getUser(user.getUserId());
        assertTrue("XXX".equals(newUser.getUserName()));
    }

}
