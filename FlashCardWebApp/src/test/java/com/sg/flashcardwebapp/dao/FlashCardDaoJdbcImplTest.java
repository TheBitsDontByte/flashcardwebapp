/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.dao;

import com.sg.flashcardwebapp.model.Card;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author feng
 */
public class FlashCardDaoJdbcImplTest {

    FlashCardDao dao;

    public FlashCardDaoJdbcImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ApplicationContext ac = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ac.getBean("cardDao", FlashCardDao.class);

        List<Card> cards = dao.getAllCards();
        for (Card card : cards) {
            dao.removeCard(card.getCardId());
        }

        Card card = new Card();
        card.setCardId(1);
        card.setQuestion("Yes?");
        card.setAnswer("yes...");
        card.setTag("no");
        dao.addCard(card);
    }

    @After
    public void tearDown() {
        List<Card> cards = dao.getAllCards();
        for (Card card : cards) {
            dao.removeCard(card.getCardId());
        }
    }

    /**
     * Test of addCard method, of class FlashCardDaoJdbcImpl.
     */
    @Test
    public void testAddGetCard() {
        Card card2 = new Card();
        card2.setQuestion("No?");
        card2.setAnswer("no...");
        card2.setTag("no");
        dao.addCard(card2);
        Card fromDao = dao.getCard(card2.getCardId());
        assertEquals(fromDao, card2);
    }

    /**
     * Test of removeCard method, of class FlashCardDaoJdbcImpl.
     */
    @Test
    public void testRemoveCard() {
        Card card2 = new Card();
        card2.setQuestion("No?");
        card2.setAnswer("no...");
        card2.setTag("no");
        dao.addCard(card2);
        List<Card> cards = dao.getAllCards();
        assertEquals(2, cards.size());
        dao.removeCard(card2.getCardId());
        cards = dao.getAllCards();
        assertEquals(1, cards.size());
    }

    /**
     * Test of updateCard method, of class FlashCardDaoJdbcImpl.
     */
    @Test
    public void testUpdateCard() {
        Card card2 = new Card();
        card2.setCardId(2);
        card2.setQuestion("No?");
        card2.setAnswer("no...");
        card2.setTag("no");
        dao.addCard(card2);
        card2.setAnswer("Yes");

    }

}
