/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.service;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author feng
 */
public class FlashCardServiceLayerTest {
    
    FlashCardServiceLayer service;
    
    public FlashCardServiceLayerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        service = ctx.getBean("service", FlashCardServiceLayer.class);
        
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getAllCardsForUser method, of class FlashCardServiceLayer.
     */
    @Test
    public void testGetAllCardsForUser() {
    }

    /**
     * Test of createConnection method, of class FlashCardServiceLayer.
     */
    @Test
    public void testCreateConnection() {
    }

    /**
     * Test of getNextNewCardId method, of class FlashCardServiceLayer.
     */
    @Test
    public void testGetNextNewCardId() {
    }

    /**
     * Test of getNextStudiedCardId method, of class FlashCardServiceLayer.
     */
    @Test
    public void testGetNextStudiedCardId() {
    }

    /**
     * Test of updateStudyDate method, of class FlashCardServiceLayer.
     */
    @Test
    public void testUpdateStudyDate() {
    }

    /**
     * Test of deleteConnection method, of class FlashCardServiceLayer.
     */
    @Test
    public void testDeleteConnection() {
    }

    /**
     * Test of getAllCards method, of class FlashCardServiceLayer.
     */
    @Test
    public void testGetAllCards() {
    }

    /**
     * Test of addCard method, of class FlashCardServiceLayer.
     */
    @Test
    public void testAddCard() {
    }

    /**
     * Test of getCard method, of class FlashCardServiceLayer.
     */
    @Test
    public void testGetCard() {
    }

    /**
     * Test of removeCard method, of class FlashCardServiceLayer.
     */
    @Test
    public void testRemoveCard() {
    }

    /**
     * Test of updateCard method, of class FlashCardServiceLayer.
     */
    @Test
    public void testUpdateCard() {
    }

    /**
     * Test of addUser method, of class FlashCardServiceLayer.
     */
    @Test
    public void testAddUser() {
    }

    /**
     * Test of getUser method, of class FlashCardServiceLayer.
     */
    @Test
    public void testGetUser() {
    }

    /**
     * Test of updateUser method, of class FlashCardServiceLayer.
     */
    @Test
    public void testUpdateUser() {
    }

    /**
     * Test of deleteUser method, of class FlashCardServiceLayer.
     */
    @Test
    public void testDeleteUser() {
    }

    /**
     * Test of getAllUsers method, of class FlashCardServiceLayer.
     */
    @Test
    public void testGetAllUsers() {
    }

    
}
