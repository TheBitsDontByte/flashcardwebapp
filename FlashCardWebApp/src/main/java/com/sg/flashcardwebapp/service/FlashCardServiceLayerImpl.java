/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.service;

import com.sg.flashcardwebapp.dao.FlashCardDao;
import com.sg.flashcardwebapp.dao.UserCardBridgeDao;
import com.sg.flashcardwebapp.dao.UserDao;
import com.sg.flashcardwebapp.model.Card;
import com.sg.flashcardwebapp.model.User;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Random;

/**
 *
 * @author feng
 */
public class FlashCardServiceLayerImpl implements FlashCardServiceLayer {

    FlashCardDao cardDao;
    UserDao userDao;
    UserCardBridgeDao userCardDao;
    private int studiedCards;

    public FlashCardServiceLayerImpl(FlashCardDao cardDao, UserDao userDao, UserCardBridgeDao userCardDao) {
        this.cardDao = cardDao;
        this.userDao = userDao;
        this.userCardDao = userCardDao;
        studiedCards = 0;

    }

    @Override
    public List<Card> getAllCardsForUser(int userId) {
        return userCardDao.getAllCardsForUser(userId);
    }

    @Override
    public void createConnection(int userId, Card card) {
        card.setAddedDate(LocalDateTime.now());
        card.setStudiedDate(null);
        card.setNextStudyDate(null);
        userCardDao.createConnection(userId, card);
    }

    @Override
    public Integer getNextCard(User user) {
        //Todo: Really figure this out
        //Todo: FIGURE THIS SHIT OUT

        Random rand = new Random();
        boolean isNewCard = rand.nextBoolean();

        if (isNewCard) {
            if () {
                return userCardDao.getNextNewCardId(user.getUserId());
            }
        }

        if (userCardDao.getNumOfCardToStudy(user.getUserId()) > 0 && studiedCards < NUM_OF_NEW_CARDS) {
            Random rand = new Random();
            return rand.nextBoolean() ? userCardDao.getNextNewCardId(user.getUserId()) : userCardDao.getNextStudiedCardId(user.getUserId());
        } else if (userCardDao.getNumOfCardToStudy(user.getUserId()) > 0) {
            return userCardDao.getNextStudiedCardId(user.getUserId());
        } else if (studiedCards < NUM_OF_NEW_CARDS) {
            return userCardDao.getNextNewCardId(user.getUserId());
        } else {
            return null;
        }
    }

    @Override
    public void updateStudyDate(int userId, Card card, String diff) {
        LocalDateTime newStudy = getPeriod(card, diff);
        card.setStudiedDate(LocalDateTime.now());
        card.setNextStudyDate(newStudy);
        userCardDao.updateStudyDate(userId, card);
    }

    private LocalDateTime getPeriod(Card card, String difficulty) {
        Difficulty diff = getDifficulty(difficulty);
        LocalDateTime studiedDate = card.getStudiedDate();
        LocalDateTime now = LocalDateTime.now();
        long periodInSecs = now.toEpochSecond(ZoneOffset.UTC) - studiedDate.toEpochSecond(ZoneOffset.UTC);
        switch (diff) {
            case HARD:
                return LocalDateTime.now().plusMinutes(1);
            case OKAY:
                return LocalDateTime.now().plusMinutes(10);
            case EASY:
                periodInSecs *= 2;
                return LocalDateTime.ofEpochSecond(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) + periodInSecs, 0, ZoneOffset.UTC);
            default:
                periodInSecs *= 4.5;
                return LocalDateTime.ofEpochSecond(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC) + periodInSecs, 0, ZoneOffset.UTC
                );
        }

    }

    private Difficulty getDifficulty(String diff) {
        switch (diff) {
            case "hard":
                return Difficulty.HARD;
            case "okay":
                return Difficulty.OKAY;
            case "easy":
                return Difficulty.EASY;
            case "veryEasy":
                return Difficulty.VERY_EASY;
            default:
                return null;
        }
    }

    @Override
    public void deleteConnection(int userId, int cardId) {
        userCardDao.deleteConnection(userId, cardId);
    }

    @Override
    public List<Card> getAllCards() {
        return cardDao.getAllCards();
    }

    @Override
    public Card addCard(Card card, int userId) {
        card.setAddedDate(LocalDateTime.now());
        card = cardDao.addCard(card);
        userCardDao.createConnection(userId, card);
        card.setAddedDate(LocalDateTime.now());
        return card;
    }

    @Override
    public Card getCard(int id) {
        return cardDao.getCard(id);
    }

    @Override
    public void removeCard(int id) {
        cardDao.removeCard(id);
    }

    @Override
    public Card updateCard(Card card) {
        return cardDao.updateCard(card);
    }

    @Override
    public User addUser(User user) {
        return userDao.addUser(user);
    }

    @Override
    public User getUser(int userId) {
        return userDao.getUser(userId);
    }

    @Override
    public void updateUser(User user) {
        userDao.updateUser(user);
    }

    @Override
    public void deleteUser(int userId) {
        List<Card> cards = userCardDao.getAllCardsForUser(userId);
        for (Card card : cards) {
            userCardDao.deleteConnection(userId, card.getCardId());
        }
        userDao.deleteUser(userId);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Override
    public List<Card> getLast20CardsForUser(int userId) {

        return this.userCardDao.getLast20CardsAdded(userId);
    }

}

enum Difficulty {
    HARD, OKAY, EASY, VERY_EASY
}
