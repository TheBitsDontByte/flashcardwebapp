/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.service;

import com.sg.flashcardwebapp.model.Card;
import com.sg.flashcardwebapp.model.User;
import java.util.List;

/**
 *
 * @author chris
 */
public interface FlashCardServiceLayer {

    public List<Card> getAllCardsForUser(int userId);

    public List<Card> getLast20CardsForUser(int userId);

    public void createConnection(int userId, Card card);

    public Integer getNextCard(User user);

    public void updateStudyDate(int userId, Card card, String diff);

    public void deleteConnection(int userId, int cardId);

    public List<Card> getAllCards();

    public Card addCard(Card card, int userId);

    public Card getCard(int id);

    public void removeCard(int id);

    public Card updateCard(Card card);

    public User addUser(User user);

    public User getUser(int userId);

    public void updateUser(User user);

    public void deleteUser(int userId);

    public List<User> getAllUsers();

}
