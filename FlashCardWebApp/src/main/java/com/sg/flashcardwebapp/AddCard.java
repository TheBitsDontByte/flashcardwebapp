/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp;

import com.sg.flashcardwebapp.dao.FCPersistenceException;
import com.sg.flashcardwebapp.model.Card;
import com.sg.flashcardwebapp.model.User;
import com.sg.flashcardwebapp.service.FlashCardServiceLayer;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author chris
 */
@Controller
@RequestMapping("/add")
public class AddCard {

    
    FlashCardServiceLayer service;
    User user = new User();


    @Inject
    public AddCard(FlashCardServiceLayer service)  {
        this.service = service;
        user.setUserId(-5);
        user.setUserName("dummy");
        user.setPassword("12345");

    }

    @RequestMapping(value = "/displayCreateCard", method = RequestMethod.GET)
    public String displayCreateCard(Model model) {
        Card card = new Card();
        model.addAttribute("card", card);
        return "add";
    }

    @RequestMapping(value = "/createCard", method = RequestMethod.POST)
    @ResponseBody
    public Card createCard(@RequestBody Map<String, String> cardMap) throws FCPersistenceException {
        Card card = new Card();
        card.setQuestion(cardMap.get("question"));
        card.setAnswer(cardMap.get("answer"));
        service.addCard(card, user.getUserId());
        return card;
    }

}
