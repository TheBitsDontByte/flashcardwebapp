/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.model;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 *
 * @author chris
 */
public class Card {

    private int cardId;
    private String question;
    private String answer;
    private String tag;
    //Used to calculate the period between studying and set the next
    private LocalDateTime nextStudyDate;
    private LocalDateTime studiedDate;
    private LocalDateTime addedDate;
 
    
    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public LocalDateTime getNextStudyDate() {
        return nextStudyDate;
    }

    public void setNextStudyDate(LocalDateTime nextStudyDate) {
        this.nextStudyDate = nextStudyDate;
    }

    public LocalDateTime getStudiedDate() {
        return studiedDate;
    }

    public void setStudiedDate(LocalDateTime studiedDate) {
        this.studiedDate = studiedDate;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + this.cardId;
        hash = 83 * hash + Objects.hashCode(this.question);
        hash = 83 * hash + Objects.hashCode(this.answer);
        hash = 83 * hash + Objects.hashCode(this.tag);
        hash = 83 * hash + Objects.hashCode(this.nextStudyDate);
        hash = 83 * hash + Objects.hashCode(this.studiedDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Card other = (Card) obj;
        if (this.cardId != other.cardId) {
            return false;
        }
        if (!Objects.equals(this.question, other.question)) {
            return false;
        }
        if (!Objects.equals(this.answer, other.answer)) {
            return false;
        }
        if (!Objects.equals(this.tag, other.tag)) {
            return false;
        }
        if (!Objects.equals(this.nextStudyDate, other.nextStudyDate)) {
            return false;
        }
        if (!Objects.equals(this.studiedDate, other.studiedDate)) {
            return false;
        }
        return true;
    }

    public LocalDateTime getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(LocalDateTime addedDate) {
        this.addedDate = addedDate;
    }

}
