/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.model;

import java.util.Objects;

/**
 *
 * @author chris
 */
public class User {

    private int userId;
    private int numOfNewCardsToStudy;
    private String userName;
    private String password;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getNumOfNewCardsToStudy() {
        return numOfNewCardsToStudy;
    }

    public void setNumOfNewCardsToStudy(int numOfNewCardsToStudy) {
        this.numOfNewCardsToStudy = numOfNewCardsToStudy;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + this.userId;
        hash = 61 * hash + this.numOfNewCardsToStudy;
        hash = 61 * hash + Objects.hashCode(this.userName);
        hash = 61 * hash + Objects.hashCode(this.password);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final User other = (User) obj;
        if (this.userId != other.userId) {
            return false;
        }
        if (this.numOfNewCardsToStudy != other.numOfNewCardsToStudy) {
            return false;
        }
        if (!Objects.equals(this.userName, other.userName)) {
            return false;
        }
        if (!Objects.equals(this.password, other.password)) {
            return false;
        }
        return true;
    }

}
