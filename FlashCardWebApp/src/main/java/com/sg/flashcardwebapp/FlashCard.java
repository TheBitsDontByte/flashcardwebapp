/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp;

import com.sg.flashcardwebapp.dao.FCPersistenceException;
import com.sg.flashcardwebapp.model.Card;
import com.sg.flashcardwebapp.service.FlashCardServiceLayer;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author chris
 */
@Controller
public class FlashCard {

    FlashCardServiceLayer service;

    @Inject
    public FlashCard(FlashCardServiceLayer service) {
        this.service = service;
        List<Card> cards = service.getAllCards();
    }

    @RequestMapping("/")
    public String home() {
        return "home";
    }

    @RequestMapping("/study")
    public String study() {
        return "study";
    }

    @RequestMapping("/add")
    public String add() {
        return "add";
    }

    @RequestMapping(value = "/showAll", method = RequestMethod.GET)
    @ResponseBody
    public List showAll() throws FCPersistenceException {
        List<Card> cards = service.getLast20CardsForUser(-5);
        return cards;
    }

}
