/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.dao;

import com.sg.flashcardwebapp.model.User;
import java.util.List;

/**
 *
 * @author chris
 */
public interface UserDao {

    public User addUser(User user);

    public User getUser(int userId);

    public void updateUser(User user);

    public void deleteUser(int userId);

    public List<User> getAllUsers();

}
