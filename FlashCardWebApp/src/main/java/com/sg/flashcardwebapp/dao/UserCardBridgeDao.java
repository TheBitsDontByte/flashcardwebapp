/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.dao;

import com.sg.flashcardwebapp.model.Card;
import java.util.List;

/**
 *
 * @author chris
 */
public interface UserCardBridgeDao {

    public Integer getNumOfCardToStudy(int userId);

    public List<Card> getAllCardsForUser(int userId);

    public List<Card> getLast20CardsAdded(int userId);

    public void createConnection(int userId, Card card);

    public void deleteConnection(int userId, int cardId);

    public Integer getNextNewCardId(int userId);

    public Integer getNextStudiedCardId(int userId);

    public void updateStudyDate(int userId, Card card);

}
