/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.dao;

import com.sg.flashcardwebapp.model.Card;
import java.util.List;

/**
 *
 * @author chris
 */
public interface FlashCardDao {

    public List<Card> getAllCards();

    public Card addCard(Card card);

    public Card getCard(int id);

    public void removeCard(int id);

    public Card updateCard(Card card);

}
