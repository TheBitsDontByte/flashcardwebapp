/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.dao;

import com.sg.flashcardwebapp.mapper.CardMapper;
import com.sg.flashcardwebapp.model.Card;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author feng
 */
public class FlashCardDaoJdbcImpl implements FlashCardDao {

    JdbcTemplate jdbcTemplate;

    public FlashCardDaoJdbcImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

//    public void setJdbcTemplate(JdbcTemplate jdbc) {
//        this.jdbcTemplate= jdbc;
//    }
    private static final String SQL_INSERT_CARD
            = "insert into Cards (question, answer, tag) "
            + "values(?,?,?)";

    private static final String SQL_SELECT_ALL_CARDS
            = "select * from Cards";

    private static final String SQL_SELECT_CARD
            = "select * from Cards where card_id = ?";

    private static final String SQL_UPDATE_CARD
            = "update Cards set question = ?, answer = ?, tag = ? "
            + "where card_id = ?";

    private static final String SQL_DELETE_CARD
            = "delete from Cards where card_id = ?";

    @Override
    public List<Card> getAllCards() {
        return jdbcTemplate.query(SQL_SELECT_ALL_CARDS, new CardMapper());
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Card addCard(Card card) {
        jdbcTemplate.update(SQL_INSERT_CARD,
                card.getQuestion(),
                card.getAnswer(),
                card.getTag());
        card.setCardId(jdbcTemplate.queryForObject("select LAST_INSERT_Id()", Integer.class));
        return card;
    }

    @Override
    public Card getCard(int id) {
        try {
            return jdbcTemplate.queryForObject(SQL_SELECT_CARD, new CardMapper(), id);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public void removeCard(int id) {
        jdbcTemplate.update(SQL_DELETE_CARD, id);
    }

    @Override
    public Card updateCard(Card card) {
        jdbcTemplate.update(SQL_UPDATE_CARD,
                card.getQuestion(),
                card.getAnswer(),
                card.getTag(),
                card.getCardId());
        return card;
    }

}
