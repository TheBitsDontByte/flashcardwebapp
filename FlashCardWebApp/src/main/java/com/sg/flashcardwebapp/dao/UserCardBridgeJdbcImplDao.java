/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.dao;

import com.sg.flashcardwebapp.mapper.CardMapper;
import com.sg.flashcardwebapp.model.Card;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author chris
 */
public class UserCardBridgeJdbcImplDao implements UserCardBridgeDao {

    JdbcTemplate jdbcTemplate;

    public UserCardBridgeJdbcImplDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    private static final String SQL_GET_CARDS_FOR_USER
            = "select c.* from Cards c "
            + " inner join user_card_bridge ucb on ucb.card_id = c.card_id "
            + "where ucb.user_id = ?";

    private static final String SQL_LAST_20_CARDS_FOR_USER
            = "select c.* from Cards c "
            + " inner join user_card_bridge ucb on ucb.card_id = c.card_id "
            + "where ucb.user_id = ? order by ucb.created_date limit 20";

    private static final String SQL_ADD_CONNECTION
            = "insert into user_card_bridge (user_id, card_id, created_date) "
            + " values (?, ?, ?)";

    private static final String SQL_DELETE_CONNECTION
            = "delete from user_card_bridge where user_id = ? and card_id = ?";

    private static final String SQL_GET_NEXT_NEW_CARD
            = "select c.card_id from Cards c "
            + " inner join user_card_bridge ucb on ucb.card_id = c.card_id "
            + " where ucb.last_studied_date is null and ucb.user_id = ? limit 1";

    private static final String SQL_GET_NEXT_REVIEW_CARD
            = "select c.card_id from Cards c "
            + " inner join user_card_bridge ucb on ucb.card_id = c.card_id "
            + " where ucb.next_study_date is not null and ucb.user_id = ?"
            + " order by ucb.next_study_date asc limit 1";

    private static final String SQL_UPDATE_STUDY_DATES
            = "update user_card_bridge set last_studied_date = ?, next_study_date = ? "
            + " where user_id = ? and card_id = ?";

    private static final String SQL_GET_NUM_OF_CARD_FOR_DATE
            = "select count(*) from user_card_bridge where date(next_study_date) = ? and user_id = ?";

    @Override
    public Integer getNumOfCardToStudy(int user_id) {
        return jdbcTemplate.queryForObject(SQL_GET_NUM_OF_CARD_FOR_DATE,
                Integer.class,
                LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy/MM/dd")),
                user_id);
    }

    @Override
    public List<Card> getAllCardsForUser(int userId) {
        return jdbcTemplate.query(SQL_GET_CARDS_FOR_USER, new CardMapper(), userId);
    }

    @Override
    public List<Card> getLast20CardsAdded(int userId) {
        return jdbcTemplate.query(SQL_LAST_20_CARDS_FOR_USER, new CardMapper(), userId);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void createConnection(int userId, Card card) {
        jdbcTemplate.update(SQL_ADD_CONNECTION, userId, card.getCardId(),
                card.getAddedDate().format(DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss"))
        );
    }

    @Override
    public void deleteConnection(int userId, int cardId) {
        jdbcTemplate.update(SQL_DELETE_CONNECTION, userId, cardId);
    }

    @Override
    public Integer getNextNewCardId(int userId) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_NEXT_NEW_CARD, Integer.class, userId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public Integer getNextStudiedCardId(int userId) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_NEXT_REVIEW_CARD, Integer.class, userId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void updateStudyDate(int userId, Card card) {
        jdbcTemplate.update(SQL_UPDATE_STUDY_DATES,
                card.getStudiedDate().format(DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss")),
                card.getNextStudyDate().format(DateTimeFormatter.ofPattern("yyyy/MM/dd hh:mm:ss")),
                userId,
                card.getCardId()
        );
    }

}
