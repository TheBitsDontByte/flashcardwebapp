/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.dao;

/**
 *
 * @author chris
 */
public class FCPersistenceException extends Exception {

    public FCPersistenceException(String message) {
        super(message);
    }

    public FCPersistenceException(String message, Throwable cause) {
        super(message, cause);
    }

}
