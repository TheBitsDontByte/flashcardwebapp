///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.sg.flashcardwebapp.dao;
//
//import com.sg.flashcardwebapp.model.Card;
//import java.io.BufferedReader;
//import java.io.File;
//import java.io.FileReader;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.time.LocalDateTime;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Scanner;
//
///**
// *
// * @author chris
// */
//public class FlashCardDaoFileImpl implements FlashCardDao {
//
//    private List<Card> cards;
//    private final String FILE;
//    private final String DELIM = "::";
//
//    public FlashCardDaoFileImpl(String file) {
//        cards = new ArrayList<>();
//        FILE = file;
//    }
//
//    @Override
//    public List<Card> getAllCards() throws FCPersistenceException {
//        cards.clear();
//        readFile();
//        return cards;
//    }
//
//    @Override
//    public Card addCard(Card card) throws FCPersistenceException {
//        cards.clear();
//        readFile();
//        cards.add(card);
//        writeFile();
//        return card;
//    }
//
//    @Override
//    public Card getCard(int id) throws FCPersistenceException {
//        cards.clear();
//        readFile();
//        //Todo: THIS NEEDS TO BE ANALYZED -- No more pulling by Id ?
//        return cards.poll();
//    }
//
//    @Override
//    public Card removeCard(int id) throws FCPersistenceException {
//        cards.clear();
//        readFile();
//        Card returnCard = cards.poll();
//        writeFile();
//        return returnCard;
//
//    }
//
//    @Override
//    public Card updateCard(Card card) throws FCPersistenceException {
//        cards.clear();
//        readFile();
//        cards.add(card);
//        writeFile();
//        return card;
//    }
//
//    private void readFile() throws FCPersistenceException {
//        Scanner scan;
//        cards.clear();
//
//        File checkFile = new File(FILE);
//        try {
//            if (checkFile.exists() == false) {
//                checkFile.createNewFile();
//            }
//        } catch (IOException e) {
//            throw new FCPersistenceException("Couldn't Load Data");
//
//        }
//
//        try {
//            scan = new Scanner(new BufferedReader(new FileReader(FILE)));
//            while (scan.hasNextLine()) {
//                String currLine = scan.nextLine();
//                String[] tokens = currLine.split(DELIM);
//
//                Card card = new Card();
//                card.setQuestion(tokens[0]);
//                card.setAnswer(tokens[1]);
//                card.setStudiedDate(LocalDateTime.parse(tokens[2]));
//                card.setNextStudyDate(LocalDateTime.parse(tokens[3]));
//
//                cards.add(card);
//            }
//
//        } catch (IOException e) {
//            throw new FCPersistenceException("Couldn't Load Data");
//        }
//
//        scan.close();
//    }
//
//    private void writeFile() throws FCPersistenceException {
//        PrintWriter out;
//
//        try {
//            out = new PrintWriter(new FileWriter(FILE));
////            List<Card> cardList = new ArrayList<>(cards.values());
//            for (Card card : cards) {
//
//                String outLine = card.getQuestion() + DELIM
//                        + card.getAnswer() + DELIM
//                        + card.getStudiedDate() + DELIM
//                        + card.getNextStudyDate();
//                out.println(outLine);
//                out.flush();
//
//            }
//        } catch (IOException e) {
//            throw new FCPersistenceException("Couldn't Save Data");
//        }
//
//        out.close();
//
//    }
//
//}
