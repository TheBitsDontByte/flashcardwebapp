/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.dao;

import com.sg.flashcardwebapp.mapper.UserMapper;
import com.sg.flashcardwebapp.model.User;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author chris
 */
public class UserJdbcImplDao implements UserDao {

    JdbcTemplate jdbcTemplate;

    public UserJdbcImplDao(JdbcTemplate jdbc) {
        this.jdbcTemplate = jdbc;
    }

    private static final String SQL_ADD_USER
            = "insert into Users (user_name, user_password) values (?, ?)";

    private static final String SQL_GET_USER
            = "select * from Users where user_id = ?";

    private static final String SQL_UPDATE_USER
            = "update Users set user_name = ?, user_password = ? where user_id = ?";

    private static final String SQL_DELETE_USER
            = "delete from Users where user_id = ?";

    private static final String SQL_GET_ALL_USERS
            = "select * from Users";

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public User addUser(User user) {
        jdbcTemplate.update(SQL_ADD_USER, user.getUserName(), user.getPassword());

        int id = jdbcTemplate.queryForObject("select LAST_INSERT_Id()", Integer.class);
        user.setUserId(id);

        return user;
    }

    @Override
    public User getUser(int userId) {
        try {
            return jdbcTemplate.queryForObject(SQL_GET_USER, new UserMapper(), userId);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    public void updateUser(User user) {
        jdbcTemplate.update(SQL_UPDATE_USER, user.getUserName(), user.getPassword(), user.getUserId());
    }

    @Override
    public void deleteUser(int userId) {
        jdbcTemplate.update(SQL_DELETE_USER, userId);
    }

    @Override
    public List<User> getAllUsers() {
        return jdbcTemplate.query(SQL_GET_ALL_USERS, new UserMapper());
    }

}
