/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp.mapper;

import com.sg.flashcardwebapp.model.Card;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author feng
 */
public class CardMapper implements RowMapper<Card> {
    @Override
    public Card mapRow(ResultSet rs, int i) throws SQLException{
        Card card = new Card();
        card.setQuestion(rs.getString("question"));
        card.setAnswer(rs.getString("answer"));
        card.setTag(rs.getString("tag"));
        card.setCardId(rs.getInt("card_id"));
        return card;
    }
    
}