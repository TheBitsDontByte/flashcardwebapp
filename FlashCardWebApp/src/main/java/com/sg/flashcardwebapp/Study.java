/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sg.flashcardwebapp;

import com.sg.flashcardwebapp.dao.FCPersistenceException;
import com.sg.flashcardwebapp.model.Card;
import com.sg.flashcardwebapp.model.User;
import com.sg.flashcardwebapp.service.FlashCardServiceLayer;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author chris
 */
@Controller
@RequestMapping("/studyCards")
public class Study {

//    Queue<Card> cards;
    List<Card> cards;
    FlashCardServiceLayer cardService;

    User user = new User();

    @Inject
    public Study(FlashCardServiceLayer cardService) throws FCPersistenceException {
        this.cardService = cardService;
        user.setUserId(-5);
        user.setUserName("dummy");
        user.setPassword("12345");
        user.setNumOfNewCardsToStudy(10);

    }

    @RequestMapping("/getNextCard")
    @ResponseBody
    public Card nextCard() throws FCPersistenceException {
//NEW DB IMPLEMENTATION
        int nextCardId = cardService.getNextCard(user);
        Card card = cardService.getCard(nextCardId);
        return card;

    }

    @RequestMapping(value = "/updateCard", method = RequestMethod.POST)
    @ResponseBody
    public Card updateCard(@RequestBody Map<String, String> cardEdits) throws FCPersistenceException {

        String editQuestion = cardEdits.get("newQuestion");
        String editAnswer = cardEdits.get("newAnswer");
        int cardId = Integer.parseInt(cardEdits.get("cardId"));
        Card card = cardService.getCard(cardId);
        card.setAnswer(editAnswer);
        card.setQuestion(editQuestion);

        cardService.updateCard(card);
        return nextCard();
    }

    @RequestMapping("/deleteCard")
    @ResponseBody
    public Card deleteCard(@RequestBody Map<String, String> cardIdMap) throws FCPersistenceException {
        String cardIdString = cardIdMap.get("cardId");
        int cardId = Integer.parseInt(cardIdString);
        cardService.deleteConnection(-5, cardId);
        return cardService.getCard(cardService.getNextCard(user));

    }

    @RequestMapping(value = "/answer", method = RequestMethod.POST)
    @ResponseBody
    public Card answer(@RequestBody Map<String, String> cardIdMap) throws FCPersistenceException {

        String cardIdString = cardIdMap.get("cardId");
        String difficulty = cardIdMap.get("difficulty");
        int cardId = Integer.parseInt(cardIdString);
        //Todo: Figure this shit out too,
        Card card = cardService.getCard(cardId);
        if (card.getStudiedDate() == null) {
            card.setStudiedDate(LocalDateTime.now());
            card.setNextStudyDate(LocalDateTime.now().plusMinutes(1));
        } else {
            Period lastPeriod = Period.between(card.getStudiedDate().toLocalDate(), card.getNextStudyDate().toLocalDate());
            card.setStudiedDate(LocalDateTime.now());
            card.setNextStudyDate(LocalDateTime.now().plusDays(lastPeriod.getDays()));
        }
        cardService.updateStudyDate(-5, card, difficulty);
        //Stupid dummy user is -5
        int nextCardId = cardService.getNextCard(user);
        return cardService.getCard(nextCardId);

    }
}
