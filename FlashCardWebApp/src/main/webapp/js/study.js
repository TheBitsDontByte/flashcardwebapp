/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {
//   $('#flash-card-back').hide();

    $('#flash-card-front').on('click', showBack);
    $('#start-study-btn').on('click', startStudying);
    $('#answer-hard-btn').click(answerHard);
    $('#answer-okay-btn').click(answerOkay);
    $('#answer-good-btn').click(answerEasy);
    $('#answer-easy-btn').click(answerVeryEasy);
    $('#edit-card-btn').click(editCard);
    $('#delete-card-btn').click(deleteCard);
    $('#save-edit-btn').click(saveEdits);
    $('#cancel-edit-btn').click(cancelEdit);
});

function startStudying() {
    $('#start-study-div').hide();
    getNextCard();
}

function getNextCard() {
    $.ajax({
        url: 'studyCards/getNextCard',
        type: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json',
        success: function (card) {
            showNextCard(card);
        },
        error: function (response) {
            showFinishedStudying();
        }

    });
}

function showNextCard(card) {
    showFront();
    $('#card-id').val(card.cardId);
    $('#card-question').text(card.question);
    $('#card-answer').text(card.answer);
}


function showFinishedStudying() {
    $('#edit-delete-row').hide("slow");
    $('#flash-card-front').hide("slow");
    $('#flash-card-back').hide("slow");
    $('#answer-row').hide("slow");
    $('#finished-study-div').show("slow");
}


function loadCards() {
    $('#flash-card-front').show();
    getNextCard();
}

function showBack() {
    $('#flash-card-back').show();
    $('#answer-row').show();
}

function showFront() {
    $('#edit-div').hide();
    $('#flash-card-front').show();
    $('#edit-delete-row').show();
    $('#answer-row').hide();
    $('#flash-card-back').hide();
}

function editCard() {
    showEdit();
    $('#edit-answer').val($('#card-answer').text());
    $('#edit-question').val($('#card-question').text());
}

function showEdit() {
    $('#flash-card-front').hide();
    $('#edit-delete-row').hide();
    $('#answer-row').hide();
    $('#flash-card-back').hide();
    $('#edit-div').show();
}

function cancelEdit() {
    showFront();
}

function saveEdits() {

    $.ajax({
        url: 'studyCards/updateCard',
        type: 'POST',
        data: JSON.stringify({
            cardId : $('#card-id').val(),
            newQuestion: $('#edit-question').val(),
            newAnswer: $('#edit-answer').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json',
        success: function (card) {
            $('#card-answer').text(card.answer);
            $('#card-question').text(card.question);
            showFront();
        },
        error: function (response) {
            //NEED TO IMPLEMENT AN ERROR THINGYMABOB
            alert("ERROR UPDATING");
        }
    });
}

function deleteCard() {
    if (!confirm("Really delete this card?"))
        return;
    $.ajax({
        url: 'studyCards/deleteCard',
        type: 'POST',
        dataType : 'json',
        data : JSON.stringify({
           cardId : $('#card-id').val() 
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        success: function (card) {
            showFront();
            showNextCard(card);
        },
        error: function (response) {
            showFinishedStudying();
        }
    });
}

function answerHard() {
    
    
    var cardId = $('#card-id').val();
    answer(cardId, "hard");
    
}

function answerOkay() {
    
    
    var cardId = $('#card-id').val();
    answer(cardId, "okay");
    
}

function answerEasy() {
    
    
    var cardId = $('#card-id').val();
    answer(cardId, "easy");
    
}

function answerVeryEasy() {
    
    
    var cardId = $('#card-id').val();
    answer(cardId, "veryEasy");
    
}




function answer (cardId, difficulty) {
    $.ajax({
        type: 'POST',
        url: 'studyCards/answer',
        data: JSON.stringify({
            difficulty : difficulty,
            cardId : cardId 
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json',
        success: function (card) {
            
            console.log("success");
            showNextCard(card);
        },

        error: function (response) {
            showFinishedStudying();
        }
    });
}


