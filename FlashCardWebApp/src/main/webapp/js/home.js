/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready( function () {
    $('#showAllCards').on('click', showAll);
});

function showAll(){
    var content = $('.displayDiv');
    console.log("yes!?");
    content.empty();
    $.ajax({
       type:'GET',
       url: 'showAll',
       success: function(card){
           $.each(card, function(index, item){
               var question= item.question;
               var answer = item.answer;
               var row = "<div class='col-md-3 cardBox'>";
               row += "<div class='row text-center'>"+ question +"</div><hr>";
               row += "<div class='row text-center'>"+ answer+"</div></div>";
               content.append(row);
           });
       },
       error: function(){
           $('#errorMsg').append("<b>Failed to load cards</b>");
       }
    });
        
    
}

