/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function(){
    $('#create-button').on('click', addCard);
});


function addCard(){
    $('#message').empty();
   console.log($('#question').val());
    console.log($('#answer').val());
   
    $.ajax({
        type: 'POST',
        url: 'createCard',
        data: JSON.stringify({
            question: $('#question').val(),
            answer: $('#answer').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json',
        success: function(){
            $('#question').val('');
            $('#answer').val('');
            $("#message").append("<b>A Card has been created Successfully</b>");
        },
        error: function(){
            $('#message').append("<b>FAIL!!!!</b>");
        }
    });
    
}
