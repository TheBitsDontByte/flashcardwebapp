<%-- 
    Document   : home
    Created on : Mar 12, 2017, 9:06:15 PM
    Author     : chris
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Study Boi</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">       
        <link href="${pageContext.request.contextPath}/css/home.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
        <style>
            body {

                background: lightblue; /* For browsers that do not support gradients */    
                background: -webkit-linear-gradient(left top, lightblue, white); /* For Safari 5.1 to 6.0 */
                background: -o-linear-gradient(bottom right, lightblue, white); /* For Opera 11.1 to 12.0 */
                background: -moz-linear-gradient(bottom right, lightblue, white); /* For Firefox 3.6 to 15 */
                background: linear-gradient(to bottom right, lightblue, white); /* Standard syntax (must be last) */
                background-attachment: fixed;
            }

            .flash-card {
                font-size: 50px;

                padding-top: 200px;
                padding: 50px;
                text-align: center;
                vertical-align: middle;
                border: 2px solid green;
                border-radius: 10px;
            }
            .flash-card-label {
                margin-top: 50px;

            }


            #answer-row, #edit-delete-row {
                padding-top: 30px;
            }

            .btn {
                width: 150px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-inverse">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNav">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="active navbar-brand glyphicon glyphicon-home" href="${pageContext.request.contextPath}/"></a>
                        </div>
                        <div class="collapse navbar-collapse" id="mainNav">
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active"><a href="${pageContext.request.contextPath}/study">Study</a></li>
                                <li><a href="${pageContext.request.contextPath}/add/displayCreateCard">Add</a></li>
                                <li><a href="#">Something</a></li>
                                <li><a href="about">About</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>

            <!--TITLE BAR STUFFS UP HERE--> 
            <div class="row" id="start-study-div">

                <div class="col-md-12">
                    <h2 class="text-center">Start Studying ?</h2>
                </div>
                <div class="col-md-12 text-center" >
                    <input type="button" class="btn btn-default" id="start-study-btn" value="Let's Go !"/>
                </div>

            </div>
            <div class="row" id="finished-study-div" style="display: none">

                <div class="col-md-12">
                    <h2 class="text-center">All Finished !!! おめでとう</h2>
                </div>
                <div class="col-md-12 text-center" >
                    <a href="/FlashCardWebApp/">
                        <input type="button" class="btn btn-default" id="finish-study-btn" value="Go Back Home"/>
                    </a>
                </div>

            </div>
            <div class="row text-center" id="edit-delete-row" style="display: none">
                <div class="col-md-2 col-md-offset-4">
                    <input type="button" id="edit-card-btn" class="btn btn-default" value="Edit Card"/>
                </div>
                <div class="col-md-2">
                    <input type="button" id="delete-card-btn" class="btn btn-default" value="Delete Card"/>
                </div>
            </div>
            <input type="hidden" name="card-id" id="card-id"/>
            <div class="row" id="flash-card-front" style="display: none">

                <div class="col-md-12 flash-card-label">
                    <h2 class="text-center">Question</h2>
                </div>
                <div class="col-md-offset-2 col-md-8 flash-card" >
                    <span id="card-question"></span>
                </div>

            </div>
            <div class="row text-center" id="answer-row" style="display: none">
                <div class="col-md-offset-2 col-md-2">
                    <input type="button" id="answer-hard-btn" class="btn btn-default" value="Hard"/>
                </div>
                <div class="col-md-2">
                    <input type="button" id="answer-okay-btn" class="btn btn-default" value="Okay"/>
                </div>
                <div class="col-md-2">
                    <input type="button" id="answer-good-btn" class="btn btn-default" value="Good"/>
                </div>
                <div class="col-md-2">
                    <input type="button" id="answer-easy-btn" class="btn btn-default" value="Easy"/>
                </div>
            </div>

            <div class="row" id="flash-card-back" style="display: none">
                <div class="col-md-12">
                    <h2 class="text-center">Answer</h2>
                </div>
                <div class="col-md-offset-2 col-md-8 flash-card" >
                    <span id="card-answer"></span>
                </div>
            </div>
            <div id="edit-div" style="display: none">
                 <div class="row" >
                    <label for="question" class="col-md-4 col-sm-4 col-xs-12 control-label">
                        Question: 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <textarea class="add-form form-control" rows="5" id="edit-question" name="question"></textarea>
                    </div>
                </div>
                <br>
                <div class="row">
                    <label for="question" class="col-md-4 col-sm-4 col-xs-12 control-label">
                        Answer: 
                    </label>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <textarea class="add-form form-control" rows="5" id="edit-answer" name="answer"></textarea>
                    </div>
                </div>
                <br>
                <div class="row">

                    <div class='col-md-offset-4 col-md-2 col-sm-offset-4 col-sm-4'>
                        <input type="button" class="btn btn-default" id="save-edit-btn" value="Save Edits"/>
                    </div>

                    <div class='col-md-4'>
                        <input type="button" class="btn btn-default" id="cancel-edit-btn" value="Cancel"/>
                    </div>
                </div>
            </div>
        </div>


    

    <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/study.js"></script>
</body>
</html>
