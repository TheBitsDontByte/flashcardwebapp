<%-- 
    Document   : home
    Created on : Mar 12, 2017, 9:06:15 PM
    Author     : chris
--%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">     
        <link href="${pageContext.request.contextPath}/css/home.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">
        <style>
            body {

                background: lightblue; /* For browsers that do not support gradients */    
                background: -webkit-linear-gradient(left top, lightblue, white); /* For Safari 5.1 to 6.0 */
                background: -o-linear-gradient(bottom right, lightblue, white); /* For Opera 11.1 to 12.0 */
                background: -moz-linear-gradient(bottom right, lightblue, white); /* For Firefox 3.6 to 15 */
                background: linear-gradient(to bottom right, lightblue, white); /* Standard syntax (must be last) */
                background-attachment: fixed;
            }

            textarea{
                resize: none;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <nav class="navbar navbar-inverse">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNav">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="active navbar-brand glyphicon glyphicon-home" href="${pageContext.request.contextPath}/"></a>
                        </div>
                        <div class="collapse navbar-collapse" id="mainNav">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="${pageContext.request.contextPath}/study">Study</a></li>
                                <li class="active" ><a href="${pageContext.request.contextPath}/add/displayCreateCard">Add</a></li>
                                <li><a href="#">Something</a></li>
                                <li><a href="about">About</a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
        <div class="container">
            <div class="text-center row">
                <span id="message"/>
            </div>
            <form class="form-horizontal" role="form" id="add-form">
                <div class="form-group">
                    <label for="question" class="col-md-4 col-sm-4 col-xs-12 control-label">
                        Question: 
                    </label>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <textarea class="add-form form-control" path="question" rows="5" id="question" name="question"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="question" class="col-md-4 col-sm-4 col-xs-12 control-label">
                        Answer: 
                    </label>
                    <div class="col-md-4 col-xs-12 col-sm-6">
                        <textarea class="add-form form-control" path="answer" rows="5" id="answer" name="answer"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class='col-md-offset-4 col-md-4 col-sm-offset-4 col-sm-4'>
                        <input type="button" class="btn btn-default" id="create-button" value="Create Card"/>
                    </div>
                </div>
            </form>
        </div>

        <script src="${pageContext.request.contextPath}/js/jquery-3.1.1.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/add.js"></script>
    </body>
</html>
