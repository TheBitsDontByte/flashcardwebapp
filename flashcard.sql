drop database if exists FlashCardTESTDB;

create database FlashCardTESTDB;

use FlashCardTESTDB;

create table Cards(
	card_id int not null auto_increment,
    question text not null,
    answer text not null,
    tag varchar(20) null,
    primary key(card_id)
)engine=innoDB default charset=latin1 auto_increment=1;

create table Users(
	user_id int not null auto_increment,
    user_name varchar(20) not null,
    user_password varchar(20) not null,
    primary key(user_id)
)engine=innoDB default charset=latin1 auto_increment=1;

create table user_card_bridge(
	user_id int not null,
    card_id int not null,
    created_date datetime null,
    last_studied_date datetime null,
    next_study_date datetime null,
    primary key(user_id, card_id),
    foreign key(card_id) references Cards(card_id),
    foreign key(user_id) references Users(user_id)
)engine=innoDB default charset=latin1 auto_increment=1;